import pandas as pd

def num_customers_per_customer_region(customer_data):
    """
    Gets the number of customers per customer region given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        customer_data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_devices_per_customer_region(customer_data, device_data):
    """
    Gets the number of devices per customer region given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        customer_data (pandas.DataFrame): The raw dataset 
        device_data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_towers_per_customer_region(tower_data):
    """
    Gets the number of towers per customer region given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        tower_data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_calls_received_per_customer_region(calls_data, tower_data):
    """
    Gets the number of calls received per customer region given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        calls_data (pandas.DataFrame): The raw dataset 
        tower_data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_calls_made_per_customer_region(calls_data, tower_data):
    """
    Gets the number of calls made per customer region given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        calls_data (pandas.DataFrame): The raw dataset 
        tower_data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def revenue_per_customer_region(**kwargs):
    """
    Gets the revenue per customer region given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    This will need to read in multiple datasets (e.g. customers/twers etc.) and aggregate the results
    to find a suitable revenue estimate.
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_devices_per_customer(data):
    """
    Gets the number of devices per customer given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_calls_per_customer(calls_data, device_data):
    """
    Gets the number of calls per customer given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        calls_data (pandas.DataFrame): The raw dataset 
        device_data (pandas.DataFrame): The raw dataset
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def num_towers_called_per_customer(calls_data, towers_data, device_data):
    """
    Gets the number of towers called per customer given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    Args:
        calls_data (pandas.DataFrame): The raw dataset 
        towers_data (pandas.DataFrame): The raw dataset 
        device_data (pandas.DataFrame): The raw dataset 
    
    Returns:
        pandas.DataFrame: result
    """
    df = pd.DataFrame()
    # START CODE HERE

    # END CODE HERE
    return df


def total_revenue_per_customer(**kwargs) -> float:
    """
    Gets the total revenue per customer given the raw dataset. 
    Note - code will have to be written to clean and process the dataset.

    This will need to read in multiple datasets (e.g. customers/twers etc.) and aggregate the results
    to find a suitable revenue estimate.
    
    Returns:
        pandas.DataFrame: result
    """
    res = None
    # START CODE HERE

    # END CODE HERE
    return res
