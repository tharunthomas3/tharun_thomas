import numpy as np


def least_moves(start: str, end: str) -> int:
    """
    Finds the least number of moves for a Knight to make on a chess board,
    to move from the start to end position.

    Args:
    ------
        start (str): the starting position on the board
        end (str): the end position on the board

    Returns:
    --------
        int: the (least) number of moves
    """
    num_moves = None
    # START CODE HERE

    # END CODE HERE
    return num_moves

