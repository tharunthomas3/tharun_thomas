# Task B: Client advisement Task

Your client, a large South African hedge fund, has requested your assistance. They have an excess of capital to invest and would appreciate your opinion regarding their allocation of investments over the next year.

> As such, they asked you to engage their South African operations.

## SA Operations

After consultation with their management, and quants, you have derived the following potential investment options in their South African money market business:

Investment details & forecast:

| Name | Currency | Distribution of %Returns | Mean | Variance | Risk of Default |
|------|----------|--------------------------|------|----------|-----------------|
| A    | ZAR      | Normal                   | 5%   | 2%       | 0%              |
| B    | ZAR      | Normal                   | 6%   | 3%       | 0%              |
| C    | ZAR      | Normal                   | 7%   | 2.5%     | 0%              |
| D    | ZAR      | Normal                   | 4%   | 1%       | 0%              |


**Each of the above metrics refer to:**
-	Name - The name of the investment
-	Currency – The currency of the investment  
-	Distribution of % returns - The predicted distribution of % change of the investment value
-	Mean – The mean of the above distribution
-	Variance - The variance of the above distribution
-	Risk of Default – The predicted risk of default of the investment – if an investment defaults, then its value becomes 0


## Client Requirements

Given the above, they would like you to provide the following:
- Functions to find the optimal portfolio weighting to acheive the following objectives:

**(a)** Best Sharpe Ratio
**(b)** Lowest Volatility 
**(c)** Best Expected Returns

Given the above:
-	Your recommendation be regarding % asset allocation between the investment options for the South African money market business, and why? 
    -  A mail to the South African manager, explaining your choice and your reasoning

Please write your emails in the `.md` mail files provided.

Note please submit any Python functions that were used to analyze the above in `utils.py` and the optimisation functions in `optimize.py` for the portfolio optimality calculations.

Please document any assumptions made in a markdown document in the repo called `assumptions.md`
