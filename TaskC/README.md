# Task C: Computational Problems

Each question has an associated `Python` script with a set of placeholder functions. There is a section for you to add your code.

* Please add your code in the **defined** scripts and functions for each question
* Please read the fucntion `docstrings` for infromation on the expected argument and return types and their descriptions.
* You can add utility/helper functions in the script freely, but the predefined functions should have your final answers/computations.
* You should only use the libraries that are already importated in the script and any Python standard libraries.
* Questions do not need to be completed in order 
* Only completed questions will be evaluated, i.e. functions that return the expected or correct results.

## Q1

Write a function that calculates the internal rate of return (IRR) for multiple series of cash flows using `Newton's Method` of root solving and determines the optimal IRR.

*  You will receive a dictionary of dictionaries as inputs.
*  Each item in the dictionary will be a (cashflow) dictionary wherein each key is a payment date, and each value is a cash flow amount

> Hint: Convert the cashflow dictionary to an array of consistent freqeuncies to be used in the `calculate_irr` function

For example:

```Python
import datetime as dt

cf_dict = {
    'a': {
        dt.datetime(2022,1,28): -1e6,
        dt.datetime(2022,2,28): 2e5,
        dt.datetime(2022,3,28): 2e5,
        dt.datetime(2022,4,28): 2e5,
        dt.datetime(2022,5,28): 2e5,
        dt.datetime(2022,6,28): 2e5,
        dt.datetime(2022,7,28): 1e5
    },
    'b': {
        dt.datetime(2022,1,28): -1e6,
        dt.datetime(2022,2,28): 2.5e5,
        dt.datetime(2022,3,28): 2.5e5,
        dt.datetime(2022,4,28): 2.5e5,
        dt.datetime(2022,5,28): 2.5e5,
        dt.datetime(2022,6,28): 1e5
    },
    'c': {
        dt.datetime(2022,1,28): -1e6,
        dt.datetime(2022,2,28): 2e5,
        dt.datetime(2022,3,28): 2e5,
        dt.datetime(2022,4,28): 2.5e5,
        dt.datetime(2022,5,28): 2.5e5,
        dt.datetime(2022,6,28): 1e5,
        dt.datetime(2022,7,28): 3e5
    }

}
# find optimal investment based on IRR
func(cf_dict)
# out
'b'
```

## Q2

Write a function that converts an `int` to its English words’ representation, for example: 

```Python
func(1_000_000)
# out
"One Million"
```

## Q3

Write a regex filter that receives a string and dictionary and returns true if all the conditions in the dictionary are met. The dictionary would have the following keys state the follows:

*  `starting` – if it starts with the values 
*  `ending` – if it ends with the values
*  `contains` – a list of substrings that need to be contained in the string
*  `case sensitive` – a Boolean stating if the above conditions are case sensitive

For example:

```Python
str_tst = 'The Event Horizon!'
dict_cond = {
    'starting': 'the',
    'ending': '!',
    'contains': ['event','horiz'],
    'case sensitive': False
}
func(str_tst, dict_cond)
# out
True
```

## Q4

Write a function that, given an `int` number N, returns a `numpy.array` of `int` 1..N arranged in a way, so the sum of each 2 consecutive numbers is a square. 

For example:

```Python
n = 9
func(n)
# out
array([1, 3, 6])
```

## Q5

Given two different positions on a chess board, find the *least number* of moves it would take a knight to get from one to the other. The positions will be passed as two arguments in algebraic notation.

* For example, knight `start="a3", end="b5"`.

The knight is not allowed to move off the board. The board is 8x8.

```Python
cord_dict = {"start": "a1", "end": "d4"}
func(**cord_dict)
# out
2
```

## Q6

Write a function that receives an 2d grid (`numpy.array`) and calculates the shortest path between a start and ending position. Please note the following regarding the array:

* `s` is the starting position
* `e` is the ending position
* `x` is impassable
* Each element in the array represents a step
* A move can either be `up`, `down`,  `left`, `right`

The output is expected to be a 2d `numpy.array` of the co-ordniates of each step made to move from start to end (i.e the shortest path).

* The co-ordinates should be `(i, j)`, where `i` is the row position and `j` the column position.

For example:

```Python
grid = np.array([['', '', '', '', ''],
       ['', 's', '', 'x', ''],
       ['', '', '', 'x', ''],
       ['', 'x', '', 'x', 'e'],
       ['', '', '', '', '']], dtype='<U1')
func(grid)
# out
array([[0, 1], [0, 2], [0, 3], [0, 4], [1, 4], [2, 4]])
```
